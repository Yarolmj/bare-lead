configure:
	sudo apt install gcc-multilib
	sudo apt install grub-common
	sudo apt-get install xorriso
	sudo apt-get install nasm
build:
	nasm -f elf32 boot.asm -o boot.o
	gcc -m32 -c lead.c -ffreestanding -nostdlib -o lead.o
	ld -m elf_i386 --nmagic --output=./iso/boot/lead.bin --script=linker.ld boot.o lead.o
	rm boot.o lead.o
	grub-mkrescue -o ./output/lead.iso iso
run:
	qemu-system-i386 -cdrom ./output/lead.iso
compress:
	zip ./output/lead.zip ./output/lead.iso

