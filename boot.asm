section .multiboot
header_start:
	;Numeros importantes
	dd 0xe85250d6 			;NUMERO MAGICO PARA MULTIBOOT
	dd 0
	dd header_end - header_start 	;Tamanio del header
	
	;checksum
	dd 0x100000000 - (0xe85250d6 + 0 + (header_end - header_start)) ;Hash GOLLO! checksum - (NUMEROMAGICO + modo + tamanio_header)
	;Tags extra
	
	;dw 4
	;dw 0
	;dd 12
	;dd 1<<1
	;align 8
	
	;dw 5
	;dw 0
	;dd 20
	;dd 320
	;dd 240
	;dd 8
	;align 8
	
	;Tag de final
	dw 0
	dw 0
	dw 8
header_end:

section .text
	extern lead_main
	global _start
_start:
    ;Colocamos el stack pointer al tope de nuestro stack
    mov esp,stack_end
    ;Llamamos a la funcion base del juego
    call lead_main
    ;Deja al procesador en halt
    cli
    hlt
Lhang:
    jmp Lhang

SECTION .bss
stack_begin:
    RESB 16384  ; Reserve 4 KiB stack space
stack_end:

