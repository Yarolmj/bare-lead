#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>

#define screen_dir 0xb8000
#define ALTO 25
#define ANCHO 80

#define time_multiplier 3

struct Sprite
{
    char characters[12]; //2 de alto x 5 de ancho +1 \n + 1 null al final
    char color;
};

struct Wall
{
    int16_t x,y; //Son necesarios numeros negativos para representar los muros medio en pantalla
    bool alive;
    bool visible;
};

struct Bullet
{
    uint8_t x,y;
    bool alive;
};

struct Entity{
    uint8_t x;
    uint8_t y;
    uint8_t dim_x,dim_y;
    uint8_t frame; //1 = forward, 2 = right, 4 = left
    bool alive;
    struct Sprite s1,s2,s3;
};


bool GOD_MODE = false;
bool player_movement = 0;
uint8_t player_lives = 3;
uint8_t inital_lives = 10;
uint32_t frame = 0;
uint32_t map_time = 0;//Registra la cantidad de updates que ha recibido el mapa desde que emepezo el stage
uint8_t Stage = 0;
uint32_t SCORE = 0;
uint32_t SCORE_BACKUP = 0;
uint8_t SATELLITE_ID = 255;
//BUffers con los caracteres y los colores respectivos
uint8_t screen_buffer[2000];
uint8_t color_buffer[2000];
uint8_t id_buffer[2000];
//Spawn range es necesario para no crear enemigos dentro o fuera de los limites del area jugable, es un rango de coordenadas en x
uint8_t spawn_range[2] = {0,0};

enum vga_color {
    COLOR_BLACK = 0,
    COLOR_BLUE = 1,
    COLOR_GREEN = 2,
    COLOR_CYAN = 3,
    COLOR_RED = 4, //Enemy Stage 1
    COLOR_MAGENTA = 5,
    COLOR_BROWN = 6,
    COLOR_LIGHT_GREY = 7,
    COLOR_DARK_GREY = 8,
    COLOR_LIGHT_BLUE = 9, //Enemy Stage 3
    COLOR_LIGHT_GREEN = 10,
    COLOR_LIGHT_CYAN = 11,
    COLOR_LIGHT_RED = 12,
    COLOR_LIGHT_MAGENTA = 13,
    COLOR_LIGHT_BROWN = 14, //Enemy Stage 2
    COLOR_WHITE = 15,
};

enum SCORE_THRESHOLD{
  FIRE_T = 10000,
  DODGE_T = 30000,
  SCRAMBLE_T = 60000,
  CATCH_T = 70000
};

enum ENEMY_COLORS{
    E_FIRE = COLOR_LIGHT_RED,
    E_DODGE = COLOR_LIGHT_BROWN,
    E_SCRAMBLE = COLOR_LIGHT_BLUE,
    E_CATCH = COLOR_LIGHT_MAGENTA,
    E_SAT = COLOR_CYAN
};
uint8_t MODE;
enum GAMEMODES{
  MENU = 0,
  GAMEPLAY = 1,
  GAMEOVER = 2
};
enum Stage_Types{
    FIRE_STAGE = 1,
    DODGE_STAGE = 2,
    SCRAMBLE_STAGE = 3,
    CATCH_STAGE = 4,
    SATELLITE = 5
};
//Estos son scancodes directos, no son ascii
//http://www.ctyme.com/intr/rb-0045.htm#Table6
enum keycodes {
	  A_PRESS = 30,
    A_DROP = 158,
    W_PRESS = 17,
    W_DROP = 145,
    S_PRESS =31,
    S_DROP = 159,
    D_PRESS = 32,
    D_DROP = 160,
    SPACE_PRESS = 57,
    SPACE_DROP = 185,
    ENTER_PRESS = 28,
    ENTER_DROP = 156
};
enum key_index{
    KEY_A = 0,
    KEY_W = 1,
    KEY_S = 2,
    KEY_D = 3,
    KEY_SPACE = 4,
    KEY_ENTER = 5
};

bool scancodes[6];//AWSD + space + ENTER

enum directions{
    UP = 1,
    RIGHT = 2,
    DOWN = 3,
    LEFT = 4
};

struct Entity player = {
    .x = 37,
    .y = 22,
    .dim_x = 5,
    .dim_y = 2,
    .frame = UP,
    .alive = true,
    .s1.characters = {32,47,79,92,32,10,124,254,124,254,124,00}, //Idle sprite
    .s1.color = COLOR_LIGHT_CYAN,
    .s2.characters = {32,47,79,92,32,10,124,254,92,254,'\\',00}, //left sprite
    .s2.color = COLOR_LIGHT_CYAN,
    .s3.characters = {32,47,79,92,32,10,'/',254,47,254,124,00}, //right sprite
    .s3.color = COLOR_LIGHT_CYAN
};

struct Sprite FIRE_STAGE_Sprites[3] = {
    {
        .characters = {' ',220,'_',220,' ','\n','|','u','w','u','|',00},
        .color = E_FIRE
    },
    {
        .characters = {' ',219,'-',219,' ','\n','\\','u','W','u','/',00},
        .color = E_FIRE
    },
    {
        .characters = {' ',220,'_',220,' ','\n','|','u','w','u','|',00},
        .color = E_FIRE
    }
};

struct Sprite DODGE_STAGE_Sprites[3] = {
    {
        .characters = {201,205,187,'\n',200,205,188,00,00,00,00,00},
        .color = E_DODGE
    },
    {
        .characters = {201,205,187,'\n',200,205,188,00,00,00,00,00},
        .color = E_DODGE
    },
    {
        .characters = {201,205,187,'\n',200,205,188,00,00,00,00,00},
        .color = E_DODGE
    }
};

struct Sprite SCRAMBLE_STAGE_Sprites[3] = {
    {
        .characters = {187,'_','_','_',201,'\n',' ',201,'_',201,' ',00},
        .color = E_SCRAMBLE
    },
    {
        .characters = {'i','_','_','_','i','\n',' ',201,'_',201,' ',00},
        .color = E_SCRAMBLE
    },
    {
        .characters = {187,'_','_','_',201,'\n',' ',201,'_',201,' ',00},
        .color = E_SCRAMBLE
    }
};

struct Sprite CATCH_STAGE_Sprites[3] = {
    {
        .characters = {'/','o',205,205,'\\','\n','\\',278,278,278,'/',00},
        .color = E_CATCH
    },
    {
        .characters = {'/',205,'o',205,'\\','\n','\\',278,278,278,'/',00},
        .color = E_CATCH
    },
    {
        .characters = {'/',205,205,'o','\\','\n','\\',278,278,278,'/',00},
        .color = E_CATCH
    }
};

struct Sprite SATELLITE_Sprites[3] = {
    {
        .characters = {201,216,187,'\n',200,216,188,00,00,00,00,00},
        .color = E_SAT
    },
    {
        .characters = {201,205,187,'\n',200,205,188,00,00,00,00,00},
        .color = E_SAT
    },
    {
        .characters = {201,216,187,'\n',200,216,188,00,00,00,00,00},
        .color = E_SAT
    }
};



struct Sprite FIRE_WALL= {
  .characters = {178,178,'\n',178,178,00},
  .color = COLOR_RED
};
struct Sprite DODGE_WALL= {
  .characters = {178,178,'\n',178,178,00},
  .color = COLOR_BLUE
};
struct Sprite SCRAMBLE_WALL= {
  .characters = {178,178,'\n',178,178,00},
  .color = COLOR_DARK_GREY
};
struct Sprite CATCH_WALL= {
  .characters = {178,178,'\n',178,178,00},
  .color = COLOR_LIGHT_RED
};
struct Sprite INVISIBLE_WALL= {
  .characters = {178,178,'\n',178,178,00},
  .color = COLOR_BLACK
};

struct Entity enemys[22] = {{.alive = false}}; //Maximo 22 enemigos en todo momento, 1 por nivel de altura
struct Bullet bullets[22] = {{.alive = false}}; //Existen maximo 22 balas en todo momento

struct Wall walls[44] = {{.alive = false,.visible = false}};

uint8_t console_line = 0; //Guarda la linea actual de la consola
#define clear_log(); console_line = 0;


struct xorshift32_state {
  uint32_t a;
};

/* Basado en Marsaglia, Xorshift RNGs */
uint32_t xorshift32(struct xorshift32_state *state)
{
	/* Algorithm "xor" from p. 4 of Marsaglia, "Xorshift RNGs" */
	uint32_t x = state->a;
	x ^= x << 13;
	x ^= x >> 17;
	x ^= x << 5;
	return state->a = x;
}

struct xorshift32_state seed;

char itoa_t1[11]; //Puede almacenar 4294967295
char itoa_t2[11];
// Funciones basicas
//Carga el "number" en string a itoa_string, el numero empieza con el identificador u
char* itoa(int number){
    int i = 0;
    do{
        itoa_t1[10 - i] = number%10 + '0';
        number = number/10;
        i++;
    }while(number != 0);
    int x = 0;
    while (x<i)
    {
        itoa_t2[x] = itoa_t1[10-i+1+x];
        x++;
    }
    itoa_t2[x] = 0;
    return itoa_t2;
}


void log(char *format,...){
    va_list vl;
    va_start(vl,format);
    uint32_t iterator = 0;
    int buffer_addr = ANCHO*console_line;
    while (format[iterator] != 0)
    {
        switch (format[iterator])
        {
        case '\n':
            console_line++;
            buffer_addr = ANCHO*console_line;
            break;
        case '%':
            iterator++;
            switch (format[iterator])
            {
            case 'u':; //unsigned integer
                char *number = itoa((int)va_arg(vl,int));
                int x = 0;
                while (number[x] != 0)
                {
                    screen_buffer[buffer_addr] = number[x];
                    color_buffer[buffer_addr]= COLOR_WHITE;
                    buffer_addr++;
                    x++;
                }
                break;
            case 'c':
                screen_buffer[buffer_addr] = (char)va_arg(vl,int);
                color_buffer[buffer_addr] = COLOR_WHITE;
                buffer_addr++;
                break;
            default:
                break;
            }
            break;
        default:
            screen_buffer[buffer_addr] = format[iterator];
            color_buffer[buffer_addr] = COLOR_WHITE;
            buffer_addr++;
            break;
        }
        iterator++;
    }
    va_end(vl);
}

void print(int x,int y,char *format,...){
    va_list vl;
    va_start(vl,format);
    uint32_t iterator = 0;
    int buffer_addr = ANCHO*y+x;
    while (format[iterator] != 0)
    {
        switch (format[iterator])
        {
        case '\n':
            y++;
            buffer_addr = ANCHO*y+x;
            break;
        case '%':
            iterator++;
            switch (format[iterator])
            {
            case 'u':; //unsigned integer
                char *number = itoa((int)va_arg(vl,int));
                int offset = 0;
                while (number[offset] != 0)
                {
                    screen_buffer[buffer_addr] = number[offset];
                    color_buffer[buffer_addr]= COLOR_WHITE;
                    buffer_addr++;
                    offset++;
                }
                break;

            default:
                break;
            }
            break;
        default:
            screen_buffer[buffer_addr] = format[iterator];
            color_buffer[buffer_addr] = COLOR_WHITE;
            buffer_addr++;
            break;
        }
        iterator++;
    }
    va_end(vl);
}

void exit(){
    __asm__(
            "mov $1,%ax\n\t"
            "int $0x80\n\t"
            "hlt\n\t"
        );
}
void wait_ms(size_t miliseconds){
	size_t iterations = miliseconds<<4;
    for (size_t i = 0; i < iterations; i++)
    {
        __asm__(
            "nop\n\t"
        );
    }
}

static inline uint8_t inb(uint16_t p)
{
    uint8_t r;
    asm("inb %1, %0" : "=a" (r) : "dN" (p));
    //log("key=%u\n",r);
    return r;
}


//HAY CODIGOS PARA PRESS Y PARA RELEASE :D
static inline uint8_t scan(){
	uint8_t key = 0;
	uint8_t scan = inb(0x60);
	if (scan != key)
		return key = scan;
	else return 0;
}

void update_scancodes(){
    uint8_t key_input = scan();
    switch (key_input)
    {
    case A_PRESS:
        scancodes[KEY_A] = true;
        break;
    case A_DROP:
        scancodes[KEY_A] = false;
        break;
    case W_PRESS:
        scancodes[KEY_W] = true;
        break;
    case W_DROP:
        scancodes[KEY_W] = false;
        break;
    case S_PRESS:
        scancodes[KEY_S] = true;
        break;
    case S_DROP:
        scancodes[KEY_S] = false;
        break;
    case D_PRESS:
        scancodes[KEY_D] = true;
        break;
    case D_DROP:
        scancodes[KEY_D] = false;
        break;
    case SPACE_PRESS:
        scancodes[KEY_SPACE] = true;
        break;
    case SPACE_DROP:
        scancodes[KEY_SPACE] = false;
        break;
    case ENTER_PRESS:
        scancodes[KEY_ENTER] = true;
        break;
    case ENTER_DROP:
        scancodes[KEY_ENTER] = false;
        break;
    default:
        break;
    }
}

//Funciones de dibujo
void render_buffer(uint8_t *buffer){
    volatile uint8_t *pixel = (volatile uint8_t*)screen_dir;
    for (size_t i = 0; i < ANCHO*ALTO; i++)
    {
        *pixel = buffer[i];
        pixel++;
        *pixel = color_buffer[i];
        pixel++;
    }

}
//Llena el buffer de color con "color"
void fill(enum vga_color color){
    for (size_t i = 0; i < ANCHO*ALTO; i++)
    {
        color_buffer[i] = color<<4|color;
        id_buffer[i] = 0;
    }
}
//Coloca un caracter "c" de color "char_color" y con fondo "back_color" en la posicion (x,y) de la pantalla
void putchar(char c,uint8_t x, uint8_t y,enum vga_color char_color,enum vga_color back_color){
    screen_buffer[y*ANCHO + x] = c;
    color_buffer[y*ANCHO + x] = back_color<<4|char_color;
}

void print_sprite(struct Sprite sprite_v,int x,int y,uint8_t id){
	uint8_t counter = 0;
	uint8_t local_x = 0;
	uint8_t local_y = 0;
	while(sprite_v.characters[counter] != 0){
		if(sprite_v.characters[counter] == '\n'){
			local_y++;
			local_x = 0;
		}else{
      if(y+local_y>=0){
        id_buffer[x+local_x+(y+local_y)*ANCHO] = id+1;
  			putchar(sprite_v.characters[counter],x+local_x,y+local_y,sprite_v.color,COLOR_BLACK);
      }
      local_x++;
		}
		counter++;
	}
}

void Draw_bullets(){
    for (size_t i = 0; i < 22; i++)
    {
        if (bullets[i].alive)
        {
            putchar(254,bullets[i].x,bullets[i].y,COLOR_RED,COLOR_BLACK);
            id_buffer[bullets[i].x+bullets[i].y*ANCHO] = 'B';
        }

    }
}

void Draw_UI(){
    print(40,24,"SCORE = %u\n",SCORE);
    print(25,24,"VIDAS = %u\n",player_lives);
    if(!player_movement){
      switch (Stage) {
        case FIRE_STAGE:
          print(30,15,"!!!SHOOT THEM ALL!!");
          break;
        case DODGE_STAGE:
          print(30,15,"!!!DODGE THEM ALL!!");
          break;
        case SCRAMBLE_STAGE:
          print(30,15,"!!!DODGE AND FIRE!!");
          break;
        case CATCH_STAGE:
          print(20,15,"!!!CATCH THE SATELLITE ESCAPE!!");
          break;
      }
    }
}
void Draw_Entity(struct Entity entity_info,uint8_t id){
  switch (entity_info.frame) {
    case 1:
      print_sprite(entity_info.s1,entity_info.x,entity_info.y,id);
      break;
    case 2:
      print_sprite(entity_info.s2,entity_info.x,entity_info.y,id);
      break;
    case 3:
      print_sprite(entity_info.s3,entity_info.x,entity_info.y,id);
      break;
    default:
      //log("ERROR:Unspected frame number %u\n",entity_info.frame);
      break;
  }
}
//Dibuja al jugador en la direccion de movimiento actual
void Draw_Player(){
    switch (player.frame)
    {
    case LEFT:
        print_sprite(player.s2,player.x,player.y,'P');
        break;
    case UP:
        print_sprite(player.s1,player.x,player.y,'P');
        break;
    case RIGHT:
        print_sprite(player.s3,player.x,player.y,'P');
        break;
    default:
        log("Not player.frame =%u\n",player.frame);
        break;
    }
}
void Draw_Enemys(){
    for (uint8_t i = 0; i < 22; i++)
    {
        if(enemys[i].alive){
          Draw_Entity(enemys[i],i);
            /*
            switch (enemys[i].frame)
            {
            case 1:
                print_sprite(enemys[i].s1,enemys[i].x,enemys[i].y,i);
                break;
            case 2:
                print_sprite(enemys[i].s2,enemys[i].x,enemys[i].y,i);
                break;
            case 3:
                print_sprite(enemys[i].s3,enemys[i].x,enemys[i].y,i);
                break;
            default:
                break;
            }
            */
        }
    }
}



//Funciones de logica del juego
char collision(uint8_t direction,struct Entity entity_info){
    switch (direction)
    {
    case UP:
        for (size_t i = 0; i < entity_info.dim_x; i++)
        {
            if(id_buffer[(entity_info.y-1)*ANCHO+entity_info.x+i] != 0){
                return id_buffer[(entity_info.y-1)*ANCHO+entity_info.x+i];
            }
        }
        break;
    case RIGHT:
        for (size_t i = 0; i < entity_info.dim_y; i++)
        {
            if(id_buffer[(entity_info.y+i)*ANCHO+entity_info.x+entity_info.dim_x+1] != 0){
                return id_buffer[(entity_info.y+i)*ANCHO+entity_info.x+entity_info.dim_x+1];
            }
        }
        break;
    case DOWN:
        for (size_t i = 0; i < entity_info.dim_x; i++)
        {
            if(id_buffer[(entity_info.y+entity_info.dim_y)*ANCHO+entity_info.x+i] != 0){
                return id_buffer[(entity_info.y+entity_info.dim_y)*ANCHO+entity_info.x+i];
            }
        }
        break;
    case LEFT:
        for (size_t i = 0; i < entity_info.dim_y; i++)
        {
            if(id_buffer[(entity_info.y+i)*ANCHO+entity_info.x-1] != 0){
                return id_buffer[(entity_info.y+i)*ANCHO+entity_info.x-1];
            }
        }
        break;
    default:
        break;
    }
    return 0;
}
//Retorna 255 si la entidad se salio de la pantalla y 0 si se movio y sigue en pantalla y collision_info si choca con alguna otra cosa
uint8_t move_entity(struct Entity *entity,uint8_t direction){
  uint8_t collision_info = collision(direction,*entity);
  uint8_t new_collision_info;
  switch (direction) {
    case UP:
      if(collision_info != 0 && collision_info != 'B'){
        new_collision_info = collision(LEFT,*entity);
        if (new_collision_info != 0) {
          new_collision_info = collision(RIGHT,*entity);
          if(new_collision_info != 0){
            return collision_info;
          }else{
            entity->x++;
            return collision_info;
          }
        }else{
          entity->x--;
          return collision_info;
        }
      }
      break;
    case RIGHT:
      if(entity->x+entity->dim_x > ANCHO){
        return 255;
      }
      if(collision_info != 0){
        if(collision_info-1 == 'W'){
          entity->x--;
          return collision_info;
        }else{
          return collision_info;
        }
      }else{
        entity->x++;
        return collision_info;
      }
      break;
    case DOWN:
      if(entity->y+entity->dim_y+1 > ALTO){
        return 255;
      }
      if(collision_info != 0){
        collision_info = collision(RIGHT,*entity);
        if(collision_info != 0){
          collision_info = collision(LEFT,*entity);
          if(collision_info != 0){
            return collision_info;//No se puede mover, choco solo :v
          }else{
            entity->y++;
            entity->x--;
          }
        }else{
          entity->y++;
          entity->x++;
        }
      }else{
        entity->y++;
      }
      break;
    case LEFT:
      if(entity->x-1 < 0){
        return 255;
      }
      if(collision_info != 0){
        if(collision_info-1 == 'W'){
          entity->x++;
          return collision_info;
        }else{
          return collision_info;
        }
      }else{
        entity->x--;
        return collision_info;
      }
      break;
  }
  return 0;
}

void update_enemys(){
    for (size_t i = 0; i < 22; i++)
    {
        if(enemys[i].alive){
            enemys[i].frame++;
            if(enemys[i].frame>3){
                enemys[i].frame = 1;
            }
            uint8_t collision_info = move_entity(&enemys[i],DOWN);
            switch (enemys[i].s1.color)
            {
            case E_FIRE:;
              if(collision_info == 255){
                enemys[i].alive = false;
                player.alive = false;
                player_lives--;
              }
              break;
            case E_DODGE:
              if(collision_info == 255){
                enemys[i].alive = false;
                SCORE+=400;
              }
              if(frame%64 == 0){
                int mov_dir = (player.x-enemys[i].x)>0;
                if(mov_dir){
                  move_entity(&enemys[i],RIGHT);
                }else{
                  move_entity(&enemys[i],LEFT);
                }
              }
              break;
            case E_SCRAMBLE:
              if(collision_info == 255){
                enemys[i].alive = false;
                player_lives--;
                player.alive = false;
                break;
              }
              if(frame%8 == 0){
                int mov_dir = (player.x-enemys[i].x)>0;
                if(mov_dir){
                  move_entity(&enemys[i],RIGHT);
                }else{
                  move_entity(&enemys[i],LEFT);
                }
              }
            case E_CATCH:
              if(collision_info == 255){
                enemys[i].alive = false;
              }
              break;
            case E_SAT:
              if(collision_info == 255){
                enemys[i].alive = false;
                player_lives--;
                player.alive = 0;
                break;
              }
              break;
            default:
                break;
            }

        }
    }

}
//Inicializa una bala
bool shoot(){
    //Comprueba que se puede disparar
    for (size_t i = 0; i < 22; i++)
    {
        if(bullets[i].alive == true && bullets[i].y == (player.y-1)){
            return false;
        }
    }
    //Si llega aqui es que puede disparar
    for (size_t i = 0; i < 22; i++)
    {
        if (bullets[i].alive == false && color_buffer[player.x+(player.y-1)*ANCHO+2] == 0) //Busca una bala "muerta" y que no valla a aparecer dentro de un muro :P
        {
            bullets[i].x = player.x+2;
            bullets[i].y = player.y-1;
            bullets[i].alive = true;
            return true; //Se disparo sin problemas
        }
    }
    return false; //Algo salio mal

}
//Mueve las ballas hacia adelante y las mata cuando chocan con algo
void update_bullets(){
    for (size_t i = 0; i < 22; i++)
    {
        if (bullets[i].alive == true)
        {
            if (bullets[i].y-1 < 0) //Choco con el techo
            {
                bullets[i].alive = false;
                continue;
            }
            uint8_t id = id_buffer[bullets[i].x+(bullets[i].y-1)*ANCHO];
            if (id != 0) {
                bullets[i].alive = false;
                if(id > 0 && id < 22){
                  switch (Stage) {
                    case FIRE_STAGE:
                      enemys[id-1].alive = false;
                      SCORE+=100;
                      break;
                    case SCRAMBLE_STAGE:
                      enemys[id-1].alive = false;
                      SCORE+=500;
                      break;
                    case CATCH_STAGE:
                      player.alive =false;
                      player_lives--;
                    default:
                      break;
                  }
                }
                continue;
            }
            //Collisiones con demas objetos
            bullets[i].y--;
        }
    }

}
void Create_Enemy(uint8_t type,uint8_t x,uint8_t y){
    for (size_t i = 0; i < 22; i++)
    {
        if(enemys[i].alive == false){
          enemys[i].x = x;
          enemys[i].y = y;
          enemys[i].dim_x = 5;
          enemys[i].dim_y = 2;
          enemys[i].alive = true;
          switch (type)
          {
          case FIRE_STAGE:
            enemys[i].s1 = FIRE_STAGE_Sprites[0];
            enemys[i].s2 = FIRE_STAGE_Sprites[1];
            enemys[i].s3 = FIRE_STAGE_Sprites[2];
            return;
            break;
          case DODGE_STAGE:
            enemys[i].s1 = DODGE_STAGE_Sprites[0];
            enemys[i].s2 = DODGE_STAGE_Sprites[1];
            enemys[i].s3 = DODGE_STAGE_Sprites[2];
            return;
            break;
          case SCRAMBLE_STAGE:
            enemys[i].s1 = SCRAMBLE_STAGE_Sprites[0];
            enemys[i].s2 = SCRAMBLE_STAGE_Sprites[1];
            enemys[i].s3 = SCRAMBLE_STAGE_Sprites[2];
            return;
            break;
          case CATCH_STAGE:
            enemys[i].s1 = CATCH_STAGE_Sprites[0];
            enemys[i].s2 = CATCH_STAGE_Sprites[1];
            enemys[i].s3 = CATCH_STAGE_Sprites[2];
            return;
            break;
          case SATELLITE:
            enemys[i].s1 = SATELLITE_Sprites[0];
            enemys[i].s2 = SATELLITE_Sprites[1];
            enemys[i].s3 = SATELLITE_Sprites[2];
            SATELLITE_ID = i;
            return;
          default:
            return;
            break;
          }
        }
    }

}
void Draw_Map(){
    for (size_t i = 0; i < 44; i++)
    {
        if (walls[i].alive)
        {
          if(walls[i].visible){
            switch (Stage) {
              case FIRE_STAGE:
                print_sprite(FIRE_WALL,walls[i].x,walls[i].y,'W');
                break;
              case DODGE_STAGE:
                print_sprite(DODGE_WALL,walls[i].x,walls[i].y,'W');
                break;
              case SCRAMBLE_STAGE:
                print_sprite(SCRAMBLE_WALL,walls[i].x,walls[i].y,'W');
                break;
              case CATCH_STAGE:
                print_sprite(CATCH_WALL,walls[i].x,walls[i].y,'W');
                break;
            }
          }else{
            print_sprite(INVISIBLE_WALL,walls[i].x,walls[i].y,'W');
          }
        }
    }
}
void Update_Map(){
    for (size_t i = 0; i < 44; i++)
    {
        if (walls[i].alive)
        {
            walls[i].y++;
            if(walls[i].y > 22){
                walls[i].alive = false;
                player_movement = 1;
            }
        }
    }
}
void MapGenerator(uint8_t Stage_Type){
    for (size_t i = 0; i < 44; i++)
    {
        if(!walls[i].alive){
            walls[i].visible = map_time%2 == 0;
            walls[i+1].visible = map_time%2 == 0;
            map_time++;
            switch (Stage_Type)
            {
            case FIRE_STAGE:
                walls[i].x = 20;
                walls[i+1].x = walls[i].x+40;
                walls[i].y = -2;
                walls[i+1].y = -2;
                walls[i].alive = true;
                walls[i+1].alive = true;
                spawn_range[0] = walls[i].x+2;
                spawn_range[1] = walls[i+1].x;
                return;
                break;
            case DODGE_STAGE:
                if(map_time == 20){
                  map_time = 0;
                }
                if(map_time >= 0 && map_time < 10){
                  walls[i].x = 10 + map_time;
                }else{
                  walls[i].x = 29 - map_time;
                }
                walls[i+1].x = walls[i].x+40;
                walls[i].y = -2;
                walls[i+1].y = -2;
                walls[i].alive = true;
                walls[i+1].alive = true;
                spawn_range[0] = walls[i].x+2;
                spawn_range[1] = walls[i+1].x;
                return;
                break;
            case SCRAMBLE_STAGE:
                if(map_time == 40){
                  map_time = 0;
                }
                if(map_time >= 0 && map_time < 20){
                  walls[i].x = 20 + map_time;
                }else{
                  walls[i].x = 59 - map_time;
                }
                walls[i+1].x = walls[i].x+25;
                walls[i].y = -2;
                walls[i+1].y = -2;
                walls[i].alive = true;
                walls[i+1].alive = true;
                spawn_range[0] = walls[i].x+2;
                spawn_range[1] = walls[i+1].x;
                return;
                break;
            case CATCH_STAGE:
                if(map_time == 10){
                  map_time = 0;
                }
                if(map_time >= 0 && map_time < 5){
                  walls[i].x = 15 + map_time;
                }else{
                  walls[i].x = 24 - map_time;
                }
                walls[i+1].x = walls[i].x+40;
                walls[i].y = -2;
                walls[i+1].y = -2;
                walls[i].alive = true;
                walls[i+1].alive = true;
                spawn_range[0] = walls[i].x+2;
                spawn_range[1] = walls[i+1].x;
                return;
                break;
            default:
                break;
            }
        }
    }
}
void GameDirector(){
    if(frame%4 == 0){

        Update_Map();
    }
    if(frame%3 == 0){
      update_enemys();
    }
    if(frame%8 == 0){
      MapGenerator(Stage);
      switch (Stage)
      {
      case FIRE_STAGE:;
        if(player_movement & xorshift32(&seed)%10 < 5){
            uint8_t Spawn_x = xorshift32(&seed)%(spawn_range[1]-spawn_range[0]-5)+spawn_range[0];
            Create_Enemy(FIRE_STAGE,Spawn_x,0);
        }
        break;
      case DODGE_STAGE:;
        if(player_movement & xorshift32(&seed)%10 <= 1){
            uint8_t Spawn_x = xorshift32(&seed)%(spawn_range[1]-spawn_range[0]-5)+spawn_range[0];
            Create_Enemy(DODGE_STAGE,Spawn_x,0);
        }
        break;
      case SCRAMBLE_STAGE:;
        if(player_movement & xorshift32(&seed)%10 <= 1){
            uint8_t Spawn_x = xorshift32(&seed)%(spawn_range[1]-spawn_range[0]-5)+spawn_range[0];
            Create_Enemy(SCRAMBLE_STAGE,Spawn_x,0);
        }
        break;
      case CATCH_STAGE:;
        if(Stage == CATCH_STAGE && SCORE>=CATCH_T && SATELLITE_ID == 255){
          uint8_t Spawn_x = xorshift32(&seed)%(spawn_range[1]-spawn_range[0]-5)+spawn_range[0];
          Create_Enemy(SATELLITE,Spawn_x,0);
          break;
        }
        if(player_movement & xorshift32(&seed)%10 <= 3){
            uint8_t Spawn_x = xorshift32(&seed)%(spawn_range[1]-spawn_range[0]-5)+spawn_range[0];
            Create_Enemy(CATCH_STAGE,Spawn_x,0);
        }
        break;
      default:
          break;
      }
    }

}
void reset_stage(){
  for (size_t i = 0; i < 6; i++) {
    scancodes[i]=false;
  }
  SATELLITE_ID = 255;
  player.alive = true;
  player.x = 37;
  player.frame = UP;
  player_movement = false;
  for (size_t i = 0; i < 22; i++) {
    enemys[i].alive = false;
    bullets[i].alive = false;
    walls[i*2].alive = false;
    walls[i*2+1].alive = false;
  }
  spawn_range[0] = 0;
  spawn_range[1] = 0;
  map_time = 0;
  if(Stage == FIRE_STAGE){
    SCORE = 0;
  }
}
#if defined(__cplusplus)
extern "C"
#endif
void lead_main(){
    int wait_time = 160000*time_multiplier;
    int input_refresh_rate = 1920;
    int remaning_time = 0;
    seed.a = 42;
    MODE = MENU;
    SCORE= 0;
    uint8_t mov_dir = UP;
    while (true)
    {
        //update_scancodes();
        //log("MODE=%u\n",MODE);
        //log("player.alive=%u\n",player.alive);
        switch (MODE) {
          case MENU:
            //wait_ms(160000);
            fill(COLOR_BLACK);
            print(0,0, "################################################################################");
            print(0,1, "################################################################################");
            print(0,2, "#                                                                              #");
            print(0,3, "#                                                                              #");
            print(0,4, "#                       @@      @@@@@@@  @@@@@  @@@@@@                         #");
            print(0,5, "#                       @@      @@      @@   @@ @@   @@                        #");
            print(0,6, "#                       @@      @@@@@   @@@@@@@ @@   @@                        #");
            print(0,7, "#                       @@      @@      @@   @@ @@   @@                        #");
            print(0,8, "#                       @@@@@@@ @@@@@@@ @@   @@ @@@@@@                         #");
            print(0,9, "#                                                                              #");
            print(0,10,"#                                                                              #");
            print(0,11,"#                                                                              #");
            print(0,12,"################################################################################");
            print(0,13,"################################################################################");
            print(0,14,"##############                                                  ################");
            print(0,15,"##############             BY YAROL MONTOYA JIMENEZ             ################");
            print(0,16,"##############                                                  ################");
            print(0,17,"##############          SISTEMAS OPERATIVOS EMPOTRADOS          ################");
            print(0,18,"##############                                                  ################");
            print(0,19,"##############          PROF. ERNESTO RIVERA ALVARADO           ################");
            print(0,20,"##############                                                  ################");
            print(0,21,"##############         A & D KEYS TO MOVE, SPACE TO SHOOT       ################");
            print(0,22,"##############                                                  ################");
            print(0,23,"##########################      ENTER TO START      ############################");
            print(0,24,"################################################################################");
            /*
            print(25,10,"(LEAD)");
            print(25,11,"BY YAROL MONTOYA JIMENEZ");
            print(25,12,"SISTEMAS OPERATIVOS EMPOTRADOS");
            print(25,13,"PROF.ERNESTO RIVERA ALVARADO");
            print(18,18,"USE \"A\" & \"D\" KEYS TO MOVE, \"SPACE\" TO SHOOT");
            print(30,19,"PRESS ENTER TO START");
            */
            if (scan() == ENTER_PRESS) {
              MODE = GAMEPLAY;
              Stage = FIRE_STAGE;
              SCORE = 0;
              SCORE_BACKUP = 0;
              GOD_MODE = false;
              player_lives = inital_lives;
              reset_stage();
            }
            break;
          case GAMEPLAY:
            //log("A:%u D:%u\n",scancodes[KEY_A],scancodes[KEY_D]);
            //log("mov_dir=%u\n",mov_dir);

            mov_dir = UP;

            if(player_lives <= 0){
              MODE = GAMEOVER;
              break;
            }
            //Dibuja la situacion actual al jugador
            if(player.alive){
              Draw_Player();
              //Draw_Entity(player,'P');
            }else{
              reset_stage();
              SCORE = SCORE_BACKUP;
              break;
            }

            //log("player_lives=%u\n",player_lives);
            Draw_Enemys();
            Draw_bullets();
            Draw_Map();
            //log("map_time=%u\n",map_time);
            //log(" %u\n%u %u\n %u\n",collision(UP,player),collision(LEFT,player),collision(RIGHT,player),collision(DOWN,player));
            update_bullets();
            GameDirector();
            Draw_UI();
            if(player_movement && player.alive){
              if(scancodes[KEY_A] && scancodes[KEY_D]){
                  mov_dir = UP;
                  seed.a = xorshift32(&seed)+10;
              }else if (scancodes[KEY_A])
              {
                mov_dir = LEFT;
                seed.a = xorshift32(&seed)+20;
              }else if(scancodes[KEY_D]){
                mov_dir = RIGHT;
                seed.a = xorshift32(&seed)+12;
              }else{
                  mov_dir = UP;
                  seed.a = xorshift32(&seed)+15;
              }
              uint8_t collision_info = move_entity(&player,mov_dir);
              if(collision_info-1 == SATELLITE_ID && SATELLITE_ID != 255){
                SCORE+=25000;
                MODE = GAMEOVER;
                break;
              }
              switch (collision_info) {
                case 0:
                  player.frame = mov_dir;
                  break;
                case 'X':
                  player.frame = mov_dir;
                  break;
                case 'B':
                  player.frame = mov_dir;
                  break;
                default:
                  player.alive = false;
                  player_lives--;
                  //log("DEAD PLAYER, HIT BY %c\n",collision_info);
                  break;
              }
              if(scancodes[KEY_SPACE] && frame%4 == 0){
                      shoot();
              }

              if(SCORE>=FIRE_T && Stage == FIRE_STAGE){
                Stage = DODGE_STAGE;
                SCORE_BACKUP = SCORE;
                reset_stage();
                break;
              }
              if(SCORE>=DODGE_T && Stage == DODGE_STAGE){
                Stage = SCRAMBLE_STAGE;
                SCORE_BACKUP = SCORE;
                reset_stage();
                break;
              }
              if(SCORE>=SCRAMBLE_T && Stage == SCRAMBLE_STAGE){
                Stage = CATCH_STAGE;
                SCORE_BACKUP = SCORE;
                reset_stage();
                break;
              }
              if(Stage == CATCH_STAGE && player_movement && frame%8 == 0){
                SCORE+=100;
              }
            }
            break;
          case GAMEOVER:

            if(player.alive){
              print(30,14,"....YOU WIN....");
            }else{
              print(30,14,"....GAMEOVER....");
            }
            print(32,15,"SCORE = %u",SCORE);
            print(27,20,"PRESS ENTER TO RESTART");
            if(scan() == ENTER_DROP){
              MODE = MENU;
              //scancodes[KEY_ENTER] = false;
              //wait_ms(160000);
            }
            break;
        }
        render_buffer(screen_buffer);
        fill(COLOR_BLACK);
        clear_log();
        frame++;
        remaning_time = wait_time;
        while (remaning_time > 0) {
          update_scancodes();
          remaning_time-=wait_time/input_refresh_rate;
          wait_ms(wait_time/input_refresh_rate);
        }
    }
}
