# LEAD GAME FROM ATARI 2600 IN BARE METAL
Bare metal version of the ATARI 2600 game LEAD written in C and Asembly x86, make for boot in x86 PC using only the text buffer.
# Building
To build the game follow the next steps in order:
1. In a folder with the repository open a new terminal
2. Type the next command download and install most of the necessary libraries to compile the game:
```
$ make configure
```
3.Type the next command to compile the game and create a .iso image of the kernel:
```
$ make build
```
That should create a file called lead.iso, this file it's the one you want to emulate or flash on a usb drive to boot from real hardware.\
4. Type the next command to emulate the kernel image in qemu with the suggested settings:
```
$ make run
```
5. Enjoy!
# Controls
Press A and D to move LEFT and RIGHT, and press SPACE to shoot from the ship. The ENTER key it's use to skip the title screens.
